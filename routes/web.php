<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Security\LoginController;
use App\Http\Controllers\Security\PermissionController;
use App\Http\Controllers\Security\RoleController;
use App\Http\Controllers\Security\UserController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

#Rutas para el inicio de sesion
Route::get('login',[LoginController::class,'index'])->name('login');
Route::post('login',[LoginController::class,'login']);
Route::get('logout',[LoginController::class,'logout'])->name('logout');

#Rutas para la asignacion de rol y pagina de inicio: usa el middleware auth en el controlador
Route::get('/', [AdminController::class,'index']);
Route::get('home',[AdminController::class,'index'])->name('home');
Route::get('current',[AdminController::class,'currentUser'])->name('user.current');
Route::get('authroles',[AdminController::class,'userRoles'])->name('auth.roles');
Route::get('selectedrole',[AdminController::class,'selectedRole'])->name('roles.selected');
Route::post('assign',[AdminController::class,'assignRole'])->name('roles.assign');

Route::middleware(['auth'])->group(function(){

    Route::get('permission',[PermissionController::class,'index'])->name('permissions.list');
    Route::post('permission',[PermissionController::class,'store'])->name('permissions.create');
    Route::put('permission/{permission}',[PermissionController::class,'update'])->name('permissions.update');
    Route::get('permission/{permission}',[PermissionController::class,'show'])->name('permissions.show');
    Route::delete('permission/{permission}',[PermissionController::class,'destroy'])->name('permissions.delete');
    Route::get('tables',[PermissionController::class,'tables'])->name('permissions.table');

    Route::get('role',[RoleController::class,'index'])->name('roles.list');
    Route::post('role',[RoleController::class,'store'])->name('roles.create');
    Route::put('role/{role}',[RoleController::class,'update'])->name('roles.update');
    Route::get('role/{role}',[RoleController::class,'show'])->name('roles.show');
    Route::delete('role/{role}',[RoleController::class,'destroy'])->name('roles.delete');
    Route::post('role/{role}/permission',[RoleController::class,'savePermissions'])->name('roles.permission');

    Route::get('user',[UserController::class,'index'])->name('users.list');
    Route::post('user',[UserController::class,'store'])->name('users.create');
    Route::put('user/{user}',[UserController::class,'update'])->name('users.update');
    Route::get('user/{user}',[UserController::class,'show'])->name('users.show');
    Route::delete('user/{user}',[UserController::class,'destroy'])->name('users.delete');
    Route::post('user/{user}/photo',[UserController::class,'updatePhoto'])->name('users.picture');
    Route::post('user/{user}/role',[UserController::class,'saveRoles'])->name('users.role');
});