<?php

namespace App\Policies\Security;

use App\Models\Security\Permission;
use App\Models\Security\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    /**
     * Permiso para el metodo index para el modelo Permission
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function viewAny(User $user)
    {
        return $user->hasPermission('permissions.list');
    }

    /**
     * Permiso para el metodo show para el modelo Permission
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\Permission  $permission
     * @return boolean
     */
    public function view(User $user, Permission $permission)
    {
        return $user->hasPermission('permissions.show');
    }

    /**
     * Permiso para el metodo create para el modelo Permission
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function create(User $user)
    {
        return $user->hasPermission('permissions.create');
    }

    /**
     * Permiso para el metodo update para el modelo Permission
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\Permission  $permission
     * @return boolean
     */
    public function update(User $user, Permission $permission)
    {
        return $user->hasPermission('permissions.update');
    }

    /**
     * Permiso para el metodo delete para el modelo Permission
     *
     * @param  \App\Models\Security\User  $user
     * @param  \App\Models\Security\Permission  $permission
     * @return boolean
     */
    public function delete(User $user, Permission $permission)
    {
        return $user->hasPermission('permissions.delete');
    }

    /**
     * Permisos para el metodo tables para el modelo Permission
     *
     * @param  \App\Models\Security\User  $user
     * @return boolean
     */
    public function tables(User $user)
    {
        return $user->hasPermission('permissions.table');
    }
}
