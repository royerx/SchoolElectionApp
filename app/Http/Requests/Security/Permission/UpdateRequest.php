<?php

namespace App\Http\Requests\Security\Permission;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * determina si un usuario esta  autorizado para ejecutar este request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Obtiene un array con las validaciones
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ'_-]*)*)+$/","unique:permissions,name,".$this->route('permission')->id,"max:30"),
            'slug' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ._-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ'_-]*)*)+$/","unique:permissions,slug,".$this->route('permission')->id,"max:255"),
            'description' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúüÁÉÍÓÚÜ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúüÁÉÍÓÚÜ'_-]*)*)+$/","max:255")
        ];
    }
    
     /**
     * Obtiene un array con los mensajes para los diferentes tipos de validaciones
     *
     * @return array
     */
    public function messages()
    {
        return[
            'name.required' => 'Debe de ingresar el nombre del permiso',
            'name.regex' => 'El formato del texto es inválido',
            'name.unique' => 'El nombre ya se encuentra registrado',
            'name.max' => 'El nombre no debe de superar los 30 caracteres',
            'slug.required' => 'Debe de ingresar el módulo y la accion',
            'slug.regex' => 'El formato del texto es inválido',
            'slug.max' => 'El módulo y la acción no debe de superar los 255 caracteres',
            'slug.unique' => 'La acción para este módulo ya fue registrada',
            'description.required' => 'Debe de ingresar la descripción del permiso',
            'description.regex' => 'El formato del texto es inválido',
            'description.max' => 'La descripción no debe de superar los 255 caracteres'
        ];
    }
}
