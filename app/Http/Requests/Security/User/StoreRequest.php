<?php

namespace App\Http\Requests\Security\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * determina si un usuario esta  autorizado para ejecutar este request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Obtiene un array con las validaciones
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lastName' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ'_-]*)*)+$/","max:50"),
            'firstName' => array("required","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ'_-]*)*)+$/","max:50"),
            'dni' => array("required","unique:users","digits_between:8,20"),
            'birthday' => array("required","date"),
            'gender' => array("required",Rule::in(['Hombre', 'Mujer'])),
            'address' => array("nullable","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ'_-]*)*)+$/","max:100"),
            'phone' => array("nullable","digits_between:6,15"),
            'photo' => array("nullable","image"),
            'login' => array("required","unique:users","regex:/^([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ_-])+((\s*)+([0-9a-zA-ZñÑáéíóúÁÉÍÓÚ'_-]*)*)+$/","max:50"),
            'email' => array("required","unique:users","email","max:255"),
        ];
    }
    
    /**
     * Obtiene un array con los mensajes para los diferentes tipos de validaciones
     *
     * @return array
     */
    public function messages()
    {
        return[
            //validación apellidos
            'lastName.required' => 'Debe de ingresar el apellido del usuario',
            'lastName.regex' => 'El formato del texto es inválido',
            'lastName.max' => 'El apellido no debe de superar los 50 caracteres',
            //validacion nombres
            'firstName.required' => 'Debe de ingresar el nombre del usuario',
            'firstName.regex' => 'El formato del texto es inválido',
            'firstName.max' => 'El nombre no debe de superar los 50 caracteres',
            //validacion dni
            'dni.required' => 'Debe de ingresar el DNI del usuario',
            'dni.unique' => 'El DNI ya se encuentra registrado',
            'dni.digits_between' => 'El DNI debe de tener 8 dígitos',
            //validacion fecha de nacimiento
            'birthday.required' => 'Debe de ingresar la fecha de nacimiento del usuario',
            'birthday.date' => 'Formato de fecha incorrecto',
            //validacion  genero
            'gender.required' => 'Debe de ingresar el sexo del usuario',
            'gender.in' => 'El sexo solo debe ser Hombre o Mujer',
            //validacion dirección
            'address.regex' => 'El formato del texto es inválido',
            'address.max' => 'La dirección no debe de superar los 100 caracteres',
            //validacion telefono
            'phone.numeric' => 'Solo debe de ingresar dígitos',
            'phone.digits_between' => 'El telefono debe de tener entre 6 y 15 numeros',
            //validacion foto
            'photo.image' =>'El archivo debe de ser imagen',
            //validacion login
            'login.required' => 'Debe de ingresar el usuario',
            'login.unique' => 'El usuario ya se encuentra registrado',
            'login.regex' => 'El formato de texto es inválido',
            'login.max' => 'El usuario no debe de superar los 50 caracteres',
            //validacion email
            'email.required' => 'Debe de ingresar el correo del usuario',
            'email.unique' => 'El correo ya se encuentra registrado',
            'email.email' => 'El formato de correo es inválido',
            'email.max' => 'El correr no debe de superar los 255 caracteres',
        ];
    }
}
