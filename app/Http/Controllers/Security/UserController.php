<?php

namespace App\Http\Controllers\Security;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Security\User;
use App\Models\Security\Role;
use App\Http\Requests\Security\User\StoreRequest;
use App\Http\Requests\Security\User\UpdateRequest;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(User::class, 'user');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar ususarios
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de usuarios
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            return  response()->json(User::with('roles')->orderBy('id','DESC')->get(),200);
        }

        return redirect('home');
    }

    /**
     * Guardar un usuario
     *
     * @param  \App\Http\Requests\Security\User\StoreRequest  $request: request para guardado
     * @param  \App\Models\Security\User  $user
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y usuario guardado
     */
    public function store(StoreRequest $request,User $user)
    {
        if($request->ajax()){
            $user=$user->store($request);
            return response()->json([
                'message' => 'Usuario creado correctamente.',
                'user' => $user->load('roles')
            ],200);
        }

        return redirect('home');
    }

    /**
     * Mostrar un usuario
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Security\User  $user
     * @return \Illuminate\Http\Response: Json con el usuario encontrado
     */
    public function show(Request $request,User $user)
    {
        if($request->ajax()){
            return response()->json([
                'user' => $user->load('roles')
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar un usuario
     *
     * @param  \App\Http\Requests\Security\User\StoreRequest  $request: request para modificado
     * @param  \App\Models\Security\User  $user
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y el usuario modificado
     */
    public function update(UpdateRequest $request, User $user)
    {
        if($request->ajax()){
            $user=$user->my_update($request,$user);
            return response()->json([
                'message' => 'Usuario actualizado correctamente.',
                'user' => $user
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar un usuario
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Security\User  $user
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,User $user)
    {
        if($request->ajax()){
            $user = $user->remove($user);
            return response()->json([
                'message' => 'Usuario eliminado correctamente.',
                'user' => $user
            ],200);
        }

        return redirect('home');
    }

    #-----------------------------------
    #METODOS QUE REQUIEREN AUTORIZACION
    #-----------------------------------

    /**
     * Guardar foto del usuario
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Security\User  $user
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con el usuario, sus roles y su foto actualizada 
     */
    public function updatePhoto(Request $request,User $user)
    {
        $this->authorize('updatePhoto', $user);
        if($request->ajax()){
            if($request->photo){
                $user->picture = $user->savePicture($request->photo,$user->picture);
                $user->save();
                return response()->json([
                    'message' => 'Foto actualizada correctamente.',
                    'user' => $user->load('roles'),
                    'picture' => $user->picture
                ],200);
            }else{
                return response()->json([
                    'message' => 'Error interno',
                    'user' => $user->load('roles')
                ],500);
            }
        }

        return redirect('home');
        
    }

     /**
     * Asignar roles a un usuario
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Security\User  $user
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion con los roles asignados
     */
    public function saveRoles(Request $request,User $user)
    {
        $this->authorize('saveRoles', $user);
        if($request->ajax()){
            $roles=collect($request->input('roles'))->pluck('id');
            $user->roles()->sync($roles);
            return response()->json([
                'message' => 'Roles guardados correctamente.',
                'user' => $user->load('roles')
            ],200);
            
        }
        
        return redirect('home');
    }
}
