<?php

namespace App\Http\Controllers\Security;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/home';
    
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Vista de página del login
     *
     * @return \Illuminate\Http\Response: Vista de la página de login
     */    

    public function index()
    {
        return view('pages.login');
    }

    /**
     * Asignar el atributo para el inicio de sesion
     *
     * @return string login: Atributo para iniciar sesion
     */    

    public function username(){
        return 'login';
    }

    /**
     * Crear sesion de rol despues de autenticarse
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Security\User $User: Usuario autenticado 
     * @return void: respuesta de error si no tiene role asignado
     */    
        
    protected function authenticated(Request $request, $user)
    {
        $roles = $user->roles()->get();
        if($roles->isNotEmpty()){
            $user->setRoleSession($roles);
        }else{
            $this->guard()->logout();
            $request->session()->invalidate();
            return redirect('login')->withErrors(['login'=>'No tiene asignado un rol consulte con el admiistrador']);
        }
    }
}
