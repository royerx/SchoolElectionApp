<?php

namespace App\Http\Controllers\Security;

use App\Http\Controllers\Controller;
use App\Http\Requests\Security\Permission\StoreRequest;
use App\Http\Requests\Security\Permission\UpdateRequest;
use App\Models\Security\Permission;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Permission::class, 'permission');
    }

    #-------------------------------------------------------
    #METODOS DE RECURSOS CON AUTORIZACION: authorizeResource
    #-------------------------------------------------------

    /**
     * Listar permisos
     * 
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con lista de permisos
     */    
    public function index(Request $request)
    {
        if($request->ajax()){
            return  response()->json(Permission::orderBy('id','DESC')->get(),200);
        }

        return redirect('home');
        
    }

    /**
     * Guardar un permiso
     *
     * @param  \App\Http\Requests\Security\Permission\StoreRequest  $request: request para guardado
     * @param  \App\Models\Security\Permissio  $pemission
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y permiso guardado
     */
    public function store(StoreRequest $request,Permission $permission)
    {
        if($request->ajax()){
            $permission= Permission::create($request->all());
            return response()->json([
                'message' => 'Permiso creado correctamente.',
                'permission' => $permission
            ],200);
        }

        return redirect('home');
    }

    /**
     * Mostrar un permiso
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Security\Permission  $permission
     * @return \Illuminate\Http\Response: Json con el permiso encontrado
     */
    public function show(Request $request,Permission $permission)
    {
        if($request->ajax()){
            return response()->json([
                'permission' => $permission
            ],200);
        }

        return redirect('home');
    }

    /**
     * Modificar un permiso
     *
     * @param  \App\Http\Requests\Security\Permission\StoreRequest  $request: request para modificado
     * @param  \App\Models\Security\Permission  $permission
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion y el permiso modificado
     */
    public function update(UpdateRequest $request, Permission $permission)
    {
        if($request->ajax()){
            $permission=$permission->update($request->all());
            return response()->json([
                'message' => 'Permiso actualizado correctamente.',
                'permission' => $permission
            ],200);
        }

        return redirect('home');
    }

    /**
     * Eliminar un permiso
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Security\Permission  $permission
     * @return \Illuminate\Http\Response: Json con mensaje de confirmacion
     */
    public function destroy(Request $request,Permission $permission)
    {
        if($request->ajax()){
            $permission=$permission->delete();
            return response()->json([
                'message' => 'Permiso eliminado correctamente.',
                'permission' => $permission
            ],200);
        }

        return redirect('home');
    }

    #-----------------------------------
    #METODOS QUE REQUIEREN AUTORIZACION
    #-----------------------------------

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response: Json con las tablas encontradas
     */
    public function tables(Request $request)
    {   
        $this->authorize('tables', Permission::class);
        if($request->ajax()){
            $tables=DB::select('SHOW TABLES');
            $tables=collect(Arr::pluck($tables,'Tables_in_electiondb'));
            $filter = $tables->diff(Permission::EXCEPT_TABLES)->toArray();
            [$keys, $values] = Arr::divide($filter);
            return response()->json($values,200);
        }

        return redirect('home');
    }
    
}
