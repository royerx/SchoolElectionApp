<?php

namespace App\Models\Security;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Security\Role;

class Permission extends Model
{
    use HasFactory;

    const EXCEPT_TABLES = [
        'role_user',
        'permission_role',
        'password_resets',
        'migrations',
        'failed_jobs'
    ];
    
    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'description'
    ];

     #RELACIONES

    /**
     * Relacion de Permiso con Rol
     *
     * @return collection \App\Models\Security\Role
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    #ALMACENAMIENTO
    
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    
    #OTRAS OPERACIONES
}
