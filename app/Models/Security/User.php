<?php

namespace App\Models\Security;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use App\Models\Security\Role;
use App\Models\Security\Permission;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    const PICTURE_PATH = '/img/profiles';
    const QRCODE_PATH = '/img/qrcodes/';
    const DEFAULT_PICTURE = '/img/profiles/user.png';

    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'lastName',
        'firstName',
        'dni',
        'birthday',
        'gender',
        'address',
        'phone',
        'picture',
        'login',
        'email',
        'password',
    ];

    /**
     * Atributos que seran ocultos
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Atributos convertidos a tipos nativos
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    /**
     * Atributos calculados o generados
     *
     * @var array
     */
    protected $appends=['qrcode'];

    #RELACIONES
      
    /**
     * Relacion de Usuario con Rol  
     *
     * @return collection \App\Models\Security\Role
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    #ALMACENAMIENTO
        
    /**
     * Guardar Usuario
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Security\User
     */
    public function store($request)
     {
        if($request->photo){
            $picture = $this->savePicture($request->photo);
        }else{
            $picture=self::DEFAULT_PICTURE;
        }
        $password = bcrypt($request->input('dni'));
        $user = self::create(array_merge($request->all(),[
            'picture' => $picture, 
            'password' => $password
         ]));
        if ($user){ 
            $this->generateQr($request->input('dni'));
            return $user;
        }
         
     }
     
     /**
     * Modificar Usuario
     *
     * @param  \Illuminate\Http\Request $request
     * @param \App\Models\Security\User
     * @return \App\Models\Security\User
     */
     public function my_update($request,$user)
     {
         if($user->dni != $request->input('dni')){
            Storage::disk('public')->delete(self::QRCODE_PATH."{$user->dni}.svg");
            $this->generateQr($request->input('dni'));
         }
         if($request->input('picture')){
            $request->merge(['picture' => $user->picture]);
         }
         return self::update($request->all());
     }
     
     /**
     * Eliminar Usuario
     *
     * @param \App\Models\Security\User
     * @return \App\Models\Security\User
     */
     public function remove($user){
        Storage::disk('public')->delete(self::QRCODE_PATH."{$user->dni}.svg");
        $this->deletePicture($user->picture);
        $user->roles()->sync([]);
        return $user->delete();
     }
    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN    
    /**
     * Obtener el path del codigo QR
     *
     * @return string path
     */
    public function getQrcodeAttribute()
    {
        return self::QRCODE_PATH."{$this->dni}.svg";
    }
    #OTRAS OPERACIONES
    
    /**
     * Generar Codigo QR
     * Se ingresa una cadena y se genera su codigo QR
     * 
     * @param  string $data
     * @return void
     */
    public function generateQr($data)
    {
        QrCode::generate($data, '../public'.self::QRCODE_PATH."{$data}.svg");
    }

        
    /**
     * Guardar foto
     *
     * @param  image $photo
     * @param  string $picture
     * @return string path guardado
     */
    public function savePicture($photo,$picture = NULL)
    {
        $this->deletePicture($picture);
        $extension= $photo->extension();
        $path=$photo->storeAs(self::PICTURE_PATH,time()."_".Str::random(20).".{$extension}",'public');
        return  $path;
    }
    
    /**
     * Eliminar foto
     *
     * @param  string $picture
     */
    public function deletePicture($picture){
        if($picture && $picture!=self::DEFAULT_PICTURE){
            Storage::disk('public')->delete("$picture");
        }
    }
    
    /**
     * Verificar permiso
     *
     * @param  string $permission
     * @return boolean
     */
    public function hasPermission($permission)
    {
       
        $role = Role::whereId(session('role_id'))->first();
        if ($role->special=="all-access"){
            return true;
        }

        if ($role->special=="no-access"){
            return false;
        }

        foreach($role->permissions()->get() as $item){
            if($item->slug==$permission){
                return true;
            }
        }

        return false;

    }
    
    /**
     * Asignar rol
     *
     * @param  array \App\Models\Security\Role $roles
     * @return void
     */
    public function setRoleSession($roles){
        if($roles->count()==1){
            Session::put([
                'role_id' => $roles[0]->id,
            ]);
        }
    }
}
