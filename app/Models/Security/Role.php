<?php

namespace App\Models\Security;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Models\Security\Permission;
use App\Models\User;

class Role extends Model
{
    use HasFactory;
        
    /**
     * Atributos que permiten asignacion masiva
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'description',
        'special'
    ];
    #RELACIONES

    /**
     * Relacion de Rol con Usuario  
     *
     * @return collection \App\Models\Security\User
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * Relacion de Rol con Permiso
     *
     * @return collection \App\Models\Security\Permission
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    #ALMACENAMIENTO

    /**
     * Guardar rol
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Models\Security\Role
     */
    public function store($request)
     {
         $slug = Str::slug($request->name,'-');
         return self::create(array_merge($request->all(),[
            'slug' => $slug, 
         ]));
     }
          
     /**
     * Modificar rol
     *
     * @param  \Illuminate\Http\Request $request
     * @param \App\Models\Security\Role
     * @return \App\Models\Security\Role
     */
     public function my_update($request)
     {
         $slug = Str::slug($request->name,'-');
         return self::update(array_merge($request->only('name','description','special'),[
            'slug' => $slug, 
         ]));
     }

    #VALIDACIÓN
    
    #RECUPERACIÓN DE INFORMACIÓN
    
    #OTRAS OPERACIONES
}
