export default {
    //strict:true,
    state: {
        user: {},
        role: {},
        snackbar: {
            visible: false,
            color: "success",
            text: null,
            timeout: 5000
        },
    },
    getters: {
        user: state => state.user,
        role: state => state.role,
        snackbar: state => state.snackbar
    },
    mutations: {
        /**
         * Mostrar el snackbar con los mensajes que se quiere mostrar
         */
        showAlert(state, payload) {

            state.snackbar.text = payload.text;

            //color del snackbar
            if (payload.color) {
                state.snackbar.color = payload.color;
            }

            // Tiempo de duración
            if (payload.timeout) {
                state.snackbar.timeout = payload.timeout;
            }
            state.snackbar.visible = true;
        },

        /**
         * Cerrar el snackbar
        */
        closeAlert(state) {
            state.snackbar.visible = false;
            state.snackbar.multiline = false;
            state.snackbar.text = null;
        },

        /**
         * Setear usuario logueado
        */
        setUser(state, data) {
            state.user = data
        },

        /**
         * Setear role seleccionado
        */
        setRole(state, data) {
            state.role = data
        }
    },
    actions: {
        
    }
}