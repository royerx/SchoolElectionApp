/**
 * Mixin con metodos y variables globales
 * 
 * @export mixin
*/
export const mixin = {
    data() {
        return {
            security: {
                //permiso para menu seguridad
                securityShow:'security.show',

                //permisos submenu permisos
                permissionsList: 'permissions.list',
                permissionsCreate: 'permissions.create',
                permissionsUpdate: 'permissions.update',
                permissionsShow: 'permissions.show',
                permissionsDelete: 'permissions.delete',
                permissionsTable: 'permissions.table',

                //permisos para submenu roles
                rolesList: 'roles.list',
                rolesCreate: 'roles.create',
                rolesUpdate: 'roles.update',
                rolesShow: 'roles.show',
                rolesDelete: 'roles.delete',
                rolesPermission: 'roles.permission',

                //permisos para submenu usuarios
                usersList: 'users.list',
                usersCreate: 'users.create',
                usersUpdate: 'users.update',
                usersShow: 'users.show',
                usersDelete: 'users.delete',
                usersPicture: 'users.picture',
                usersRole: 'users.role',
            }
        }
    },
    methods: {
        /**
         * Verificar permiso
         * 
         * @param {string} permission - slug del permiso
         * @returns {boolean} 
        */
        can: function (permission) {

            const role = this.$store.getters.role
            const permissions = role.permissions

            if (role.special == "all-access") {
                return true
            }

            if (role.special == "no-access") {
                return false
            }

            if (permissions) {
                for (let element of permissions) {
                    if (element.slug == permission) {
                        return true
                    }
                }
            }
            return false
        },
    },
}