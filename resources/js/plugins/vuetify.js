import '@mdi/font/css/materialdesignicons.css'
import Vue from 'vue'
import Vuetify from 'vuetify/lib'

//Importacion de paquetes de idiomas de vuetify
import es from 'vuetify/es5/locale/es'
import en from 'vuetify/es5/locale/en'

Vue.use(Vuetify)

//Cargar los iconos y los idiomas
const opts = {
    icons: {
        iconfont: 'mdiSvg'
    },
    lang: {
        locales: { es, en },
        current: 'es'
    },

}

export default new Vuetify(opts)