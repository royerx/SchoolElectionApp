/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

//require('./bootstrap')
window._ = require('lodash');
window.axios = require('axios')
import Vue from 'vue'
import Vuex from 'vuex'
import Vuetify from './plugins/vuetify'
import {mixin} from './plugins/mixin'
import router from './helpers/router'
import { ValidationProvider, ValidationObserver } from 'vee-validate'
import { extend } from 'vee-validate'
import * as rules from 'vee-validate/dist/rules'
import { messages } from 'vee-validate/dist/locale/es.json'
import storeData from './plugins/store'
import VuePaginate from 'vue-paginate'
import { filters } from './helpers/filters'
import { initialize } from './helpers/initialize'

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'

Vue.use(Vuex)
Vue.use(VuePaginate)

const store = new Vuex.Store(storeData)

initialize(store)
filters(Vue)

Object.keys(rules).forEach(rule => {
  extend(rule, {
    ...rules[rule], // copies rule configuration
    message: messages[rule] // assign message
  })
})


Vue.prototype.$perPage = 20;
Vue.prototype.$pagesVisible = 5;
Vue.prototype.$userDefaultImgUrl = '/img/profiles/user.png';
/*Vue.prototype.$permission = new Map([
  ['role.create','role.create'],
  ['role.show','role.show']
]);*/


Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('app', require('./views/layouts/App.vue').default)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.mixin(mixin)

const app = new Vue({
  store,
  vuetify: Vuetify,
  router,
  el: '#app',
})
