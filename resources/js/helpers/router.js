import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const routes = [
    {
        path: '/seguridad-roles',
        name: 'role',
        component: () => import('../views/security/role/Index')
    },
    {
        path: '/seguridad-permisos',
        name: 'permission',
        component: () => import('../views/security/permission/Index')
    },
    {
        path: '/seguridad-usuarios',
        name: 'user',
        component: () => import('../views/security/user/Index')
    },
    {

        path: '/security',
        children: [
            {
                path: 'roles',
                component: () => import('../views/security/role/Index')
            }
        ]
    },
    {
        path: '/dashboard',
        name: 'dashboard',
        //component: ()=>import('./views/Dashboard.vue')
    },
    {
        path: '/project',
        name: 'project',
        //component: ()=>import('./views/Project.vue')
    },
    {
        path: '/team',
        name: 'team',
        //component: ()=>import('./views/Team.vue')
    }
];

const router = new Router({
    routes: routes
});

export default router