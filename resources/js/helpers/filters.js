
import moment from 'moment'
/**
 * Filtros
 * Permite generar filtros(formatos) globalmente para el uso en los templates
 * 
 * @export filtros
 * @param {object} Vue - instancia de vue
 */
export function filters(Vue) {
  //filtro para formato de fecha dia/mes/año
  Vue.filter('date', function (value) {
    if (value) {
      return moment(String(value)).format('DD/MM/YYYY')
    }
  })
  //filtro para formato de fecha y hora y minutos
  Vue.filter('dateHour', function (value) {
    if (value) {
      return moment(String(value)).format('DD/MM/YYYY hh:mm')
    }
  })
}